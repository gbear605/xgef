﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;

using System.Text.RegularExpressions;
using System.Collections;
using System.Diagnostics;

namespace XGEF.Compiler
{
	[DebuggerDisplay("ID:{ID} {Text}")]
	public partial class ScriptFile : IEnumerable<INode>
	{
		private static int IDPool;

		public string OriginalText { get; set; }
		public string Text { get { return ReconstructCode(); } }
		public Dictionary<string, INode> TopLevel { get; set; }
		public List<string> Usings { get; set; }
		public int ID { get; }

		public void AddMember(INode node)
		{
			TopLevel[node.FullName] = node;
			node.Parent = null;
			node.ScriptFile = this;
			foreach (var child in node.Children())
				node.ScriptFile = this;
		}

		public void InsertMember(INode node, string parentName)
		{
			INode parent = MemberWithName(parentName);
			if (parent == null)
			{
				AddMember(node);
			}
			else
			{
				node.Parent = parent;
				parent.AddChild(node);
				node.ScriptFile = this;
				foreach (var child in node.Children())
					child.ScriptFile = this;
			}
		}

		public bool RemoveMember(INode node)
		{
			if (TopLevel.ContainsValue(node))
			{
				node.Parent = null;
				return TopLevel.Remove(node.FullName);
			}

			foreach(var member in Members())
			{
				bool result = member.RemoveChild(node);
				if (result)
				{
					node.Parent = null;
					return true;
				}
			}

			return false;
		}

		public string ReconstructCode()
		{
			string result = "";
			if(Usings != null && Usings.Count > 0)
				result += string.Join("", Usings.ToArray());
			foreach (INode node in TopLevel.Values)
			{
				result += node.ReconstructCode();
			}
			return result;
		}

		public string GetDiagnostics()
		{
			string result =  string.Join("", Usings.ToArray());

			foreach (Namespace name in TopLevel.Values)
			{
				result += name.GetDiagnostics();
			}
			return result;
		}

		public INode MemberWithName(string fullname)
		{
			return Members().ToList().Where(x => x.FullName == fullname).FirstOrDefault();
		}

		public IEnumerable<INode> MembersWithName(string name)
		{
			return Members().ToList().Where(x => x.Name == name);
		}

		public IEnumerable<INode> MembersOfType(MemberType type)
		{
			return Members().ToList().Where(x => x.Type == type);
		}

		public IEnumerable<INode> MembersWithAttributes()
		{
			return Members().ToList().Where(x => x.HasAttributes);
		}

		public IEnumerable<INode> MembersWithAttribute(string name)
		{
			return Members().ToList().Where(x => x.HasAttribute(name));
		}

		public virtual IEnumerable<INode> Members()
		{
			foreach (INode node in TopLevel.Values)
			{
				yield return node;
				foreach (INode value in node.Descendants())
					yield return value;
			}
		}

		public IEnumerator<INode> GetEnumerator()
		{
			foreach (INode node in Members())
			{
				yield return node;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public ScriptFile()
		{
			TopLevel = new Dictionary<string, INode>();
			Usings = new List<string>();
			ID = IDPool++;
		}
	}

	public interface INode : IEnumerable<INode>
	{
		string Filename { get; }
		string Name { get; }
		string FullName { get; }
		INode Parent { get; set; }
		void AddChild(INode node);
		ScriptFile ScriptFile { get; set; }
		MemberType Type { get; }
		List<Attribute> Attributes { get; }
		bool HasAttributes { get; }
		bool HasAttribute(string name);
		Attribute GetAttribute(string name);
		IEnumerable<Attribute> GetAttributes(string name);
		bool IsExpression { get; set; }

		T GetParent<T>() where T : class, INode;

		string Declaration { get; set; }
		string Text { get; set; }
		string FullBody { get; }
		string Body { get; }
		string Surround(string body);
		string GetCode();
		string ReconstructCode();
		string GetDiagnostics(int tabs);
		IEnumerable<INode> Descendants();
		IEnumerable<INode> Children();
		bool RemoveChild(INode node);
	}

	public abstract class Node : INode
	{
		public virtual string Filename { get; set; }
		public virtual string Name { get; set; }
		public virtual string FullName { get { return $"{(Parent?.FullName == null ? "" : Parent.FullName + ".")}{Name}"; } }
		public virtual INode Parent { get; set; }
		public virtual void AddChild(INode node) { }
		public ScriptFile ScriptFile { get; set; }
		public MemberType Type { get; set; }
		public List<Attribute> Attributes { get; set; }
		public virtual bool HasAttributes { get { return Attributes.Count > 0; } }
		public virtual bool HasAttribute(string name)
		{
			return Attributes.Any(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute"));
		}
		public virtual Attribute GetAttribute(string name)
		{
			return Attributes.Where(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute")).FirstOrDefault();
		}

		public virtual IEnumerable<Attribute> GetAttributes(string name)
		{
			return Attributes.Where(x => x.Name.Equals(name) || x.Name.Equals(name + "Attribute"));
		}
		public virtual void AddAttribute(Attribute attrib) { Attributes.Add(attrib); }
		public bool IsExpression { get; set; }

		protected virtual List<Type> ValidParents { get; }
		public virtual T GetParent<T>()
			where T : class, INode
		{
			if (ValidParents.Contains(typeof(T)))
				return Parent as T;

			return null;
		}

		public virtual string Declaration { get; set; }
		public virtual string Text { get; set; }
		public virtual string Body { get { return Text; } }
		public virtual string FullBody { get { return Surround(Body); } }
		public virtual string Surround(string body) { return Body; }

		public Node()
		{
			ValidParents = new List<Type>();
			Attributes = new List<Attribute>();
		}

		public virtual string GetCode()
		{
			return Declaration + (Text ?? "");
		}

		public virtual string ReconstructCode()
		{
			return string.Join("", Attributes.Select(x => x.Declaration)) + GetCode();
		}

		public virtual string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			return $"{result}{Type}: {FullName}\n";
		}

		public virtual IEnumerator<INode> GetEnumerator()
		{
			foreach (var child in Children())
			{
				yield return child;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public virtual IEnumerable<INode> Descendants()
		{
			foreach (INode node in Children())
			{
				yield return node;
				foreach (INode value in node.Descendants())
					yield return value;
			}
		}

		public virtual IEnumerable<INode> Children()
		{
			yield break;
		}

		public virtual bool RemoveChild(INode node) { return false; }

		public override string ToString()
		{
			return $"{Type}:{FullName}";
		}
	}

	public class Namespace : Node
	{
		public Namespace GetParent() { return Parent as Namespace; }
		
		public Dictionary<string, Class> Classes { get; }
		public Dictionary<string, Enum> Enums { get; }
		public Dictionary<string, Class> Structs { get; }
		public Dictionary<string, Field> Delegates { get; }
		public Dictionary<string, Class> Interfaces { get; }
		public Dictionary<string, Namespace> Namespaces { get; }

		public void AddClass(Class node) { Classes[node.FullName] = node; }
		public void AddEnum(Enum node) { Enums[node.FullName] = node; }
		public void AddStruct(Class node) { Structs[node.FullName] = node; }
		public void AddDelegate(Field node) { Delegates[node.FullName] = node; }
		public void AddInterface(Class node) { Interfaces[node.FullName] = node; }
		public void AddNamespace(Namespace node) { Namespaces[node.FullName] = node; }

		public override void AddChild(INode node)
		{
			switch (node.Type)
			{
				case MemberType.Class:
					AddClass(node as Class);
					break;
				case MemberType.Struct:
					AddStruct(node as Class);
					break;
				case MemberType.Interface:
					AddInterface(node as Class);
					break;
				case MemberType.Enum:
					AddEnum(node as Enum);
					break;
				case MemberType.Delegate:
					AddDelegate(node as Field);
					break;
				case MemberType.Namespace:
					AddNamespace(node as Namespace);
					break;
				case MemberType.Attribute:
					AddAttribute(node as Attribute);
					break;
			}
		}

		public override IEnumerable<INode> Children()
		{
			foreach (INode node in Namespaces.Values)
				yield return node;

			foreach (INode node in Enums.Values)
				yield return node;

			foreach (INode node in Delegates.Values)
				yield return node;

			foreach (INode node in Interfaces.Values)
				yield return node;

			foreach (INode node in Classes.Values)
				yield return node;

			foreach (INode node in Structs.Values)
				yield return node;
		}

		public override bool RemoveChild(INode node)
		{
			if (Namespaces.ContainsKey(node.FullName))
				return Namespaces.Remove(node.FullName);

			if (Enums.ContainsKey(node.FullName))
				return Enums.Remove(node.FullName);

			if (Delegates.ContainsKey(node.FullName))
				return Delegates.Remove(node.FullName);

			if (Interfaces.ContainsKey(node.FullName))
				return Interfaces.Remove(node.FullName);

			if (Classes.ContainsKey(node.FullName))
				return Classes.Remove(node.FullName);

			if (Structs.ContainsKey(node.FullName))
				return Structs.Remove(node.FullName);

			return false;
		}

		public override string Body
		{
			get
			{
				string result = "";
				foreach (INode node in this)
				{
					result += node.ReconstructCode();
				}
				return result;
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (Text.Contains("{"))
			{
				result += Regex.Match(Text, @"^\s*\{\n?", RegexOptions.Singleline);
			}

			result += body;

			if (Text.Contains("}"))
			{
				var matches = Regex.Matches(Text, @"\s*\}\s*$", RegexOptions.Singleline);
				if (matches.Count > 0)
					result += matches[matches.Count - 1];
			}
			return result;
		}

		public override string ReconstructCode()
		{
			string result = string.Join("", Attributes.Select(x => x.Declaration)) + Declaration;
			result += Surround(Body);
			return result;
		}

		public override string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			result += $"{Type}: {FullName}:\n";

			foreach (INode node in Children())
			{
				result += node.GetDiagnostics(tabs + 1);
			}

			return result;
		}

		public Namespace()
		{
			Classes = new Dictionary<string, Class>();
			Enums = new Dictionary<string, Enum>();
			Structs = new Dictionary<string, Class>();
			Delegates = new Dictionary<string, Field>();
			Interfaces = new Dictionary<string, Class>();
			Namespaces = new Dictionary<string, Namespace>();
			ValidParents.AddRange(new[] { typeof(Namespace) } );
		}
	}

	public class Class : Node
	{
		public Dictionary<string, Method> Constructors { get; }
		public Dictionary<string, Field> Constants { get; }
		public Dictionary<string, Field> Fields { get; }
		public Dictionary<string, Method> Finalizers { get; }
		public Dictionary<string, Method> Methods { get; }
		public Dictionary<string, Property> Properties { get; }
		public Dictionary<string, Property> Indexers { get; }
		public Dictionary<string, Method> Operators { get; }
		public Dictionary<string, Field> Events { get; }
		public Dictionary<string, Field> Delegates { get; }
		public Dictionary<string, Class> Classes { get; }
		public Dictionary<string, Enum> Enums { get; }
		public Dictionary<string, Class> Structs { get; }
		public Dictionary<string, Class> Interfaces { get; }
		
		public void AddConstructor(Method node)
		{
			//Done differently on purpose; any constructors are messily stored since parsing them down to ObjectName(int, int) or equivalent
			// is more difficult than the utility gained.
			Constructors[node.Declaration] = node;
		}
		public void AddConstant(Field node) { Constants[node.FullName] = node; }
		public void AddField(Field node) { Fields[node.FullName] = node; }
		public void AddInterface(Class node) { Interfaces[node.FullName] = node; }
		public void AddFinalizer(Method node) { Finalizers[node.FullName] = node; }
		public void AddMethod(Method node) { Methods[node.FullName] = node; }
		public void AddProperty(Property node) { Properties[node.FullName] = node; }
		public void AddIndexer(Property node) { Indexers[node.FullName] = node; }
		public void AddOperator(Method node) { Operators[node.FullName] = node; }
		public void AddEvent(Field node) { Events[node.FullName] = node; }
		public void AddDelegate(Field node) { Delegates[node.FullName] = node; }
		public void AddClass(Class node) { Classes[node.FullName] = node; }
		public void AddEnum(Enum node) { Enums[node.FullName] = node; }
		public void AddStruct(Class node) { Structs[node.FullName] = node; }
		public override void AddChild(INode node)
		{
			switch (node.Type)
			{
				case MemberType.Class:
					AddClass(node as Class);
					break;
				case MemberType.Struct:
					AddStruct(node as Class);
					break;
				case MemberType.Interface:
					AddInterface(node as Class);
					break;
				case MemberType.Enum:
					AddEnum(node as Enum);
					break;
				case MemberType.Delegate:
					AddDelegate(node as Field);
					break;
				case MemberType.Event:
					AddEvent(node as Field);
					break;
				case MemberType.Method:
					AddMethod(node as Method);
					break;
				case MemberType.Constructor:
					AddConstructor(node as Method);
					break;
				case MemberType.Finalizer:
					AddFinalizer(node as Method);
					break;
				case MemberType.Operator:
					AddOperator(node as Method);
					break;
				case MemberType.Constant:
					AddConstant(node as Field);
					break;
				case MemberType.Field:
					AddField(node as Field);
					break;
				case MemberType.Property:
					AddProperty(node as Property);
					break;
				case MemberType.Indexer:
					AddIndexer(node as Property);
					break;
				case MemberType.Attribute:
					AddAttribute(node as Attribute);
					break;
			}
		}

		public override IEnumerable<INode> Children()
		{
			foreach (INode node in Enums.Values)
				yield return node;

			foreach (INode node in Interfaces.Values)
				yield return node;

			foreach (INode node in Classes.Values)
				yield return node;

			foreach (INode node in Structs.Values)
				yield return node;

			foreach (INode node in Delegates.Values)
				yield return node;

			foreach (INode node in Events.Values)
				yield return node;

			foreach (INode node in Constants.Values)
				yield return node;

			foreach (INode node in Fields.Values)
				yield return node;

			foreach (INode node in Properties.Values)
				yield return node;

			foreach (INode node in Indexers.Values)
				yield return node;

			foreach (INode node in Operators.Values)
				yield return node;

			foreach (INode node in Methods.Values)
				yield return node;

			foreach (INode node in Constructors.Values)
				yield return node;

			foreach (INode node in Finalizers.Values)
				yield return node;
		}

		public override bool RemoveChild(INode node)
		{
			if (Enums.ContainsKey(node.FullName))
				return Enums.Remove(node.FullName);
			if (Interfaces.ContainsKey(node.FullName))
				return Interfaces.Remove(node.FullName);
			if (Classes.ContainsKey(node.FullName))
				return Classes.Remove(node.FullName);
			if (Structs.ContainsKey(node.FullName))
				return Structs.Remove(node.FullName);
			if (Delegates.ContainsKey(node.FullName))
				return Delegates.Remove(node.FullName);
			if (Events.ContainsKey(node.FullName))
				return Events.Remove(node.FullName);
			if (Constants.ContainsKey(node.FullName))
				return Constants.Remove(node.FullName);
			if (Fields.ContainsKey(node.FullName))
				return Fields.Remove(node.FullName);
			if (Properties.ContainsKey(node.FullName))
				return Properties.Remove(node.FullName);
			if (Indexers.ContainsKey(node.FullName))
				return Indexers.Remove(node.FullName);
			if (Operators.ContainsKey(node.FullName))
				return Operators.Remove(node.FullName);
			if (Methods.ContainsKey(node.FullName))
				return Methods.Remove(node.FullName);
			//remember that constructors are wonky
			if (Constructors.ContainsKey(node.Declaration))
				return Constructors.Remove(node.Declaration);
			if (Finalizers.ContainsKey(node.FullName))
				return Finalizers.Remove(node.FullName);

			return false;
		}

		public override string Body
		{
			get
			{
				string result = "";
				foreach (INode node in this)
				{
					result += node.ReconstructCode();
				}
				return result;
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (Text.Contains("{"))
			{
				result += Regex.Match(Text, @"^\s*\{\n?", RegexOptions.Singleline);
			}

			result += body;

			if (Text.Contains("}"))
			{
				var matches = Regex.Matches(Text, @"\s*\}\s*$", RegexOptions.Singleline);
				if (matches.Count > 0)
					result += matches[matches.Count - 1];
			}
			return result;
		}

		public override string ReconstructCode()
		{
			string result = string.Join("", Attributes.Select(x => x.Declaration)) + Declaration;
			result += Surround(Body);
			return result;
		}

		public override string GetDiagnostics(int tabs = 0)
		{
			string result = new string('\t', tabs);
			result += $"{Type}: {FullName}:\n";

			foreach (INode node in this)
			{
				result += node.GetDiagnostics(tabs + 1);
			}

			return result;
		}

		public Class()
		{
			Constructors = new Dictionary<string, Method>();
			Constants = new Dictionary<string, Field>();
			Fields = new Dictionary<string, Field>();
			Finalizers = new Dictionary<string, Method>();
			Methods = new Dictionary<string, Method>();
			Properties = new Dictionary<string, Property>();
			Indexers = new Dictionary<string, Property>();
			Operators = new Dictionary<string, Method>();
			Events = new Dictionary<string, Field>();
			Delegates = new Dictionary<string, Field>();
			Classes = new Dictionary<string, Class>();
			Enums = new Dictionary<string, Enum>();
			Structs = new Dictionary<string, Class>();
			Interfaces = new Dictionary<string, Class>();
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class) });
		}
	}

	public class Enum : Node
	{
		public Dictionary<string, string> Values { get; set; }

		public override string Body
		{
			get
			{
				var pairs = Values.Select(x => string.Join("", x.Key, x.Value));
				return string.Join(", ", pairs.ToArray());
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (Text.Contains("{"))
			{
				result += Regex.Match(Text, @"^\s*\{\s?", RegexOptions.Singleline);
			}

			result += body;

			if (Text.Contains("}"))
			{
				var matches = Regex.Matches(Text, @"\s*\}\s*$", RegexOptions.Singleline);
				if (matches.Count > 0)
					result += matches[matches.Count - 1];
			}
			return result;
		}

		public override string ReconstructCode()
		{
			string result = string.Join("", Attributes.Select(x => x.Declaration)) + Declaration;
			result += Surround(Body);
			return result;
		}

		public Enum()
		{
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class) });
			Values = new Dictionary<string, string>();
		}
	}

	public class Attribute : Node
	{
		public List<string> Arguments { get; set; }

		public Attribute()
		{
			Arguments = new List<string>();
			ValidParents.AddRange(new[] { typeof(Namespace), typeof(Class), typeof(Field), typeof(Method),
				typeof(Enum), typeof(Property),});
		}
	}

	public class Method : Node
	{
		public List<string> Modifiers { get; set; }
		public string ReturnType { get; set; }
		public string DisplayName { get; set; }
		public string Parameters { get; set; }
		public string Indent { get; set; }

		public override string Declaration
		{
			get
			{
				return $"{Indent}{string.Join(" ", Modifiers.ToArray())} {ReturnType}{DisplayName}{Parameters}";
			}
		}

		public override string Body
		{
			get
			{
				string body = Text ?? "";
				body = Regex.Replace(body, @"^\s*\{\s?", "", RegexOptions.Singleline);
				body = Regex.Replace(body, @"\s*\}\s*$", "", RegexOptions.Singleline);
				return body;
			}
		}

		public override string Surround(string body)
		{
			string result = "";

			if (Text.Contains("{"))
			{
				result += Regex.Match(Text, @"^\s*\{\s?", RegexOptions.Singleline);
			}

			result += body;

			if (Text.Contains("}"))
			{
				var matches = Regex.Matches(Text, @"\s*\}\s*$", RegexOptions.Singleline);
				if (matches.Count > 0)
					result += matches[matches.Count - 1];
			}
			return result;
		}

		public override string ReconstructCode()
		{
			string result = string.Join("", Attributes.Select(x => x.Declaration)) + Declaration;
			result += Surround(Body);
			return result;
		}

		public Method()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
			Modifiers = new List<string>();
		}
	}

	public class Field : Node
	{
		public bool Constant { get; set; }
		public Field()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
		}
	}

	public class Property : Node
	{
		public Property()
		{
			ValidParents.AddRange(new[] { typeof(Class) });
		}
	}

	
}
