﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Text.RegularExpressions;

namespace XGEF.Compiler
{
	public partial class ScriptFile
	{
		public static string DefaultNamespace = "DefaultNamespace";

		//These aren't overloads of the Tree versions because else you need to include references
		// to Roslyn to use it, even if you don't access the overloaded version that requires a SyntaxTree
		public static ScriptFile AnalyzeFile(string relativeFilename, string content)
		{
			string filtered = FilterText(content);
			return AnalyzeTree(relativeFilename, CSharpSyntaxTree.ParseText(filtered));
		}

		public static IEnumerable<ScriptFile> AnalyzeFiles(string relativeFilename, params string[] files)
		{
			var Files = new List<ScriptFile>();

			foreach (var file in files)
			{
				Files.Add(AnalyzeFile(relativeFilename, file));
			}

			return Files;
		}

		public static string FilterText(string script)
		{
			script = Regex.Replace(script, @"\r", "");
			script = Regex.Replace(script, @"//.*", "");
			script = Regex.Replace(script, @"/\*.*?\*/", "", RegexOptions.Singleline);
			script = Regex.Replace(script, @"#region.*", "");
			script = Regex.Replace(script, @"#endregion.*", "");
			script = Regex.Replace(script, @"\n(\s*\n)+", "\n", RegexOptions.Singleline);

			return script;
		}

		public static ScriptFile AnalyzeTree(string filename, SyntaxTree tree)
		{
			var root = tree.GetCompilationUnitRoot();
			if (root == null)
				throw new Exception("somehow this tree ain't got no compilation unit");

			return ParseRoot(root);
		}

		public static IEnumerable<ScriptFile> AnalyzeTrees(string filename, List<SyntaxTree> trees)
		{
			var Files = new List<ScriptFile>();

			foreach (var tree in trees)
			{
				Files.Add(AnalyzeTree(filename, tree));
			}

			return Files;
		}

		private static string GetSection(SyntaxTree tree, int startPos, int endPos)
		{
			//return tree.GetText().GetSubText(new TextSpan(startPos, endPos)).ToString();
			return tree.GetText().ToString().Substring(startPos, endPos - startPos);
		}

		private static ScriptFile ParseRoot(CompilationUnitSyntax root)
		{
			ScriptFile file = new ScriptFile
			{
				OriginalText = root.ToFullString()
			};

			var children = root.ChildNodes();

			foreach (var member in children)
			{
#if CS_7
				switch (member)
				{
					case UsingDirectiveSyntax x:
						file.Usings.Add(x.GetText().ToString());
						break;

					case NamespaceDeclarationSyntax x:
						file.AddMember(ParseNamespace(x, null));
						break;

					case ClassDeclarationSyntax x:
						file.AddMember(ParseClass(x, null));
						break;

					case DelegateDeclarationSyntax x:
						file.AddMember(ParseDelegate(x, null));
						break;

					case InterfaceDeclarationSyntax x:
						file.AddMember(ParseInterface(x, null));
						break;

					case StructDeclarationSyntax x:
						file.AddMember(ParseStruct(x, null));
						break;

					case EnumDeclarationSyntax x:
						file.AddMember(ParseEnum(x, null));
						break;

				}
#else
				if (member is UsingDirectiveSyntax)
				{
					var x = member as UsingDirectiveSyntax;
					file.Usings.Add(x.GetText().ToString());
				}
				if (member is NamespaceDeclarationSyntax)
				{
					var x = member as NamespaceDeclarationSyntax;
					file.AddMember(ParseNamespace(x, null));
				}
				if (member is ClassDeclarationSyntax)
				{
					var x = member as ClassDeclarationSyntax;
					file.AddMember(ParseClass(x, null, MemberType.Class));
				}
				else if (member is InterfaceDeclarationSyntax)
				{
					var x = member as InterfaceDeclarationSyntax;
					file.AddMember(ParseClass(x, null, MemberType.Interface));
				}
				else if (member is StructDeclarationSyntax)
				{
					var x = member as StructDeclarationSyntax;
					file.AddMember(ParseClass(x, null, MemberType.Struct));
				}
				else if (member is DelegateDeclarationSyntax)
				{
					var x = member as DelegateDeclarationSyntax;
					file.AddMember(ParseField(x, null, MemberType.Delegate));
				}
				else if (member is EnumDeclarationSyntax)
				{
					var x = member as EnumDeclarationSyntax;
					file.AddMember(ParseEnum(x, null));
				}
#endif

			}

			foreach (var member in file.Members())
			{
				member.ScriptFile = file;
			}

			return file;
		}

		private static List<Attribute> FindAttributes(SyntaxNode node, INode parent)
		{
			IEnumerable<AttributeListSyntax> attribList = null;
			//Namespaces technically aren't allowed to have attributes, but for our purposes we will allow them.
			//This requires us to find any attributes ourselves, as the SyntaxTree rejects them.
			if (node is NamespaceDeclarationSyntax)
			{
				//trick the SyntaxTree by making it look like an assembly-level attribute
				string trivia = node.GetLeadingTrivia().ToFullString().Replace("[", "[assembly: ");
				var root = CSharpSyntaxTree.ParseText(trivia).GetRoot();
				attribList = root.ChildNodes().OfType<AttributeListSyntax>();
			}
			else
			{
				attribList = node.ChildNodes().OfType<AttributeListSyntax>();
			}

			List<Attribute> result = new List<Attribute>();

			foreach(var alist in attribList)
			{
				foreach(var attrib in alist.Attributes)
				{
					Attribute a = new Attribute()
					{
						Name = attrib.Name.ToString(),
						Declaration = alist.GetText().ToString().Replace("[assembly: ", "["),
						Parent = parent,
						Type = MemberType.Attribute
					};

					result.Add(a);

					parent.Declaration = parent.Declaration.Replace(a.Declaration, "");

					var args = attrib.DescendantNodes().OfType<AttributeArgumentListSyntax>().FirstOrDefault();

					if (args is default)
						continue;

					foreach(var arg in args.Arguments)
					{
						a.Arguments.Add(arg.GetText().ToString());
					}
				}
			}

			

			return result;
		}

		private static Namespace ParseNamespace(NamespaceDeclarationSyntax node, INode parent)
		{
			Namespace result = new Namespace()
			{
				Name = node.Name.ToString(),
				Parent = parent,
				Type = MemberType.Namespace
			};

			int StartPos = node.GetFirstToken().FullSpan.Start;
			int BodyStart = node.OpenBraceToken.FullSpan.Start;
			int BodyEnd = node.GetLastToken().FullSpan.End;

			result.Declaration = GetSection(node.SyntaxTree, StartPos, BodyStart);
			result.Text = GetSection(node.SyntaxTree, BodyStart, BodyEnd);

			foreach (var member in node.Members)
			{
#if CS_7
				switch (member)
				{
					case NamespaceDeclarationSyntax x:
						result.AddNamespace(ParseNamespace(x, result));
						break;

					case ClassDeclarationSyntax x:
						result.AddClass(ParseClass(x, result));
						break;

					case DelegateDeclarationSyntax x:
						result.AddDelegate(ParseDelegate(x, result));
						break;

					case InterfaceDeclarationSyntax x:
						result.AddInterface(ParseInterface(x, result));
						break;

					case StructDeclarationSyntax x:
						result.AddStruct(ParseStruct(x, result));
						break;

					case EnumDeclarationSyntax x:
						result.AddEnum(ParseEnum(x, result));
						break;

				}
#else
				if (member is NamespaceDeclarationSyntax)
				{
					var x = member as NamespaceDeclarationSyntax;
					result.AddNamespace(ParseNamespace(x, result));
				}
				if (member is ClassDeclarationSyntax)
				{
					var x = member as ClassDeclarationSyntax;
					result.AddClass(ParseClass(x, result, MemberType.Class));
				}
				else if (member is InterfaceDeclarationSyntax)
				{
					var x = member as InterfaceDeclarationSyntax;
					result.AddInterface(ParseClass(x, result, MemberType.Interface));
				}
				else if (member is StructDeclarationSyntax)
				{
					var x = member as StructDeclarationSyntax;
					result.AddStruct(ParseClass(x, result, MemberType.Struct));
				}
				else if (member is DelegateDeclarationSyntax)
				{
					var x = member as DelegateDeclarationSyntax;
					result.AddDelegate(ParseField(x, result, MemberType.Delegate));
				}
				else if (member is EnumDeclarationSyntax)
				{
					var x = member as EnumDeclarationSyntax;
					result.AddEnum(ParseEnum(x, result));
				}
#endif
				
			}

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Class ParseClass(TypeDeclarationSyntax node, INode parent, MemberType type)
		{
			Class result = new Class()
			{
				Name = node.Identifier.Text.ToString(),
				Parent = parent,
				Type = type
			};

			int StartPos = node.GetFirstToken().FullSpan.Start;
			int BodyStart = node.OpenBraceToken.FullSpan.Start;
			int BodyEnd = node.GetLastToken().FullSpan.End;

			result.Declaration = GetSection(node.SyntaxTree, StartPos, BodyStart);
			result.Text = GetSection(node.SyntaxTree, BodyStart, BodyEnd);

			foreach (var member in node.Members)
			{
#if CS_7
				switch (member)
				{
					case ClassDeclarationSyntax x:
						result.AddClass(ParseClass(x, result));
						break;

					case ConstructorDeclarationSyntax x:
						result.AddConstructor(ParseConstructor(x, result));
						break;

					case FieldDeclarationSyntax field:
						if (field.Modifiers.Any(x => x.IsKind(SyntaxKind.ConstKeyword)))
							result.AddConstant(ParseField(field, result));
						else
							result.AddField(ParseField(field, result));
						break;

					case DestructorDeclarationSyntax x:
						result.AddFinalizer(ParseFinalizer(x, result));
						break;

					case MethodDeclarationSyntax x:
						result.AddMethod(ParseMethod(x, result));
						break;

					case PropertyDeclarationSyntax x:
						result.AddProperty(ParseProperty(x, result));
						break;

					case IndexerDeclarationSyntax x:
						result.AddIndexer(ParseIndexer(x, result));
						break;

					case OperatorDeclarationSyntax x:
						result.AddOperator(ParseOperator(x, result));
						break;

					case EventFieldDeclarationSyntax x:
						result.AddEvent(ParseEvent(x, result));
						break;

					case DelegateDeclarationSyntax x:
						result.AddDelegate(ParseDelegate(x, result));
						break;

					case InterfaceDeclarationSyntax x:
						result.AddInterface(ParseInterface(x, result));
						break;

					case StructDeclarationSyntax x:
						result.AddStruct(ParseStruct(x, result));
						break;

					case EnumDeclarationSyntax x:
						result.AddEnum(ParseEnum(x, result));
						break;

				}
#else
				if (member is ClassDeclarationSyntax)
				{
					var x = member as ClassDeclarationSyntax;
					result.AddClass(ParseClass(x, result, MemberType.Class));
				}
				else if (member is InterfaceDeclarationSyntax)
				{
					var x = member as InterfaceDeclarationSyntax;
					result.AddInterface(ParseClass(x, result, MemberType.Interface));
				}
				else if (member is StructDeclarationSyntax)
				{
					var x = member as StructDeclarationSyntax;
					result.AddStruct(ParseClass(x, result, MemberType.Struct));
				}

				else if (member is MethodDeclarationSyntax)
				{
					var x = member as MethodDeclarationSyntax;
					result.AddMethod(ParseMethod(x, result, MemberType.Method));
				}
				else if (member is ConstructorDeclarationSyntax)
				{
					var x = member as ConstructorDeclarationSyntax;
					result.AddConstructor(ParseMethod(x, result, MemberType.Constructor));
				}
				else if (member is DestructorDeclarationSyntax)
				{
					var x = member as DestructorDeclarationSyntax;
					result.AddFinalizer(ParseMethod(x, result, MemberType.Finalizer));
				}
				else if (member is OperatorDeclarationSyntax)
				{
					var x = member as OperatorDeclarationSyntax;
					result.AddOperator(ParseMethod(x, result, MemberType.Operator));
				}

				else if (member is FieldDeclarationSyntax)
				{
					var field = member as FieldDeclarationSyntax;
					if (field.Modifiers.Any(x => x.IsKind(SyntaxKind.ConstKeyword)))
						result.AddConstant(ParseField(field, result, MemberType.Constant));
					else
						result.AddField(ParseField(field, result, MemberType.Field));
				}
				else if (member is EventFieldDeclarationSyntax)
				{
					var x = member as EventFieldDeclarationSyntax;
					result.AddEvent(ParseField(x, result, MemberType.Event));
				}
				else if (member is DelegateDeclarationSyntax)
				{
					var x = member as DelegateDeclarationSyntax;
					result.AddDelegate(ParseField(x, result, MemberType.Delegate));
				}

				else if (member is PropertyDeclarationSyntax)
				{
					var x = member as PropertyDeclarationSyntax;
					result.AddProperty(ParseProperty(x, result, MemberType.Property));
				}
				else if (member is IndexerDeclarationSyntax)
				{
					var x = member as IndexerDeclarationSyntax;
					result.AddIndexer(ParseProperty(x, result, MemberType.Indexer));
				}
				
				else if (member is EnumDeclarationSyntax)
				{
					var x = member as EnumDeclarationSyntax;
					result.AddEnum(ParseEnum(x, result));
				}
#endif
			}

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Method ParseMethod(BaseMethodDeclarationSyntax node, INode parent, MemberType type)
		{
			string name = node.Name();
			string displayName = name;
			if (type == MemberType.Finalizer)
				name = $"~{name}";
			else if(type == MemberType.Operator)
				name = $"Operator{(node as OperatorDeclarationSyntax).OperatorToken.ToString()}";

			Method result = new Method()
			{
				Name = name,
				DisplayName = displayName,
				Parent = parent,
				Type = type
			};
			foreach(var mod in node.Modifiers)
			{
				result.Modifiers.Add(mod.Text.ToString());
			}

			if(node is MethodDeclarationSyntax method)
			{
				result.ReturnType = method.ReturnType.GetText().ToString();
			}

			result.Parameters = node.ParameterList.GetText().ToString();

			//foreach(var param in node.ParameterList.Parameters)
			//{
			//	result.Parameters.Add($"{param.Type.ToString()} {param.Identifier.ToString()}");
			//}
			
			int StartPos = node.GetFirstToken().FullSpan.Start;
			int EndPos = 0;
			if (node.Body != null)
			{
				var block = node.ChildNodes().OfType<BlockSyntax>().First();
				EndPos = block.OpenBraceToken.FullSpan.Start;
				result.Text = block.GetText().ToString();
			}
			else if (node.ExpressionBody != null)
			{
				result.IsExpression = true;
				var arrow = node.ChildNodes().OfType<ArrowExpressionClauseSyntax>().First();
				EndPos = arrow.FullSpan.Start;
				result.Text = GetSection(node.SyntaxTree, EndPos, node.GetLastToken().FullSpan.End);
			}
			else
			{
				EndPos = node.GetLastToken().FullSpan.End;
				result.Text = "";
			}
			result.Indent = Regex.Match(GetSection(node.SyntaxTree, StartPos, EndPos), @"^\s+").ToString();

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Property ParseProperty(BasePropertyDeclarationSyntax node, INode parent, MemberType type)
		{
			string name = node.Name();
			if(type == MemberType.Indexer)
			{
				var param = node.ChildNodes().OfType<BracketedParameterListSyntax>().First();
				name = param.Parameters.First().Type.ToString() + "Indexer";
			}

			Property result = new Property()
			{
				Name = name,
				Parent = parent,
				Type = type
			};

			int StartPos = node.GetFirstToken().FullSpan.Start;
			int BodyStart = 0;
			int BodyEnd = 0;
			if(node.ChildNodes().OfType<ArrowExpressionClauseSyntax>().Count() > 0)
			{
				result.IsExpression = true;
				var arrow = node.ChildNodes().OfType<ArrowExpressionClauseSyntax>().First();
				BodyStart = arrow.FullSpan.Start;
				BodyEnd = node.GetLastToken().FullSpan.End;
			}
			else
			{
				BodyStart = node.ChildNodes().OfType<AccessorListSyntax>().First().GetFirstToken().FullSpan.Start;
				BodyEnd = node.GetLastToken().FullSpan.End;
			}
			
			result.Declaration = GetSection(node.SyntaxTree, StartPos, BodyStart);
			result.Text = GetSection(node.SyntaxTree, BodyStart, BodyEnd);

			result.Attributes = FindAttributes(node, result);

			return result;
		}

		private static Field ParseField(MemberDeclarationSyntax node, INode parent, MemberType type)
		{
			Field result = new Field()
			{
				Name = node.Name(),
				Parent = parent,
				Type = type
			};

			if (result.Name == null)
			{
				var declarator = node.DescendantNodes().OfType<VariableDeclaratorSyntax>().First();
				result.Name = declarator.Identifier.ToString();
			}
			result.Declaration = node.GetText().ToString();

			result.Attributes = FindAttributes(node, result);
			return result;
		}

		private static Enum ParseEnum(EnumDeclarationSyntax node, INode parent)
		{
			Enum result = new Enum()
			{
				Name = node.Name(),
				Parent = parent,
				Type = MemberType.Enum
			};

			int StartPos = node.GetFirstToken().FullSpan.Start;
			int BodyStart = node.OpenBraceToken.FullSpan.Start;
			int BodyEnd = node.GetLastToken().FullSpan.End;

			result.Declaration = GetSection(node.SyntaxTree, StartPos, BodyStart);
			result.Text = GetSection(node.SyntaxTree, BodyStart, BodyEnd);

			result.Attributes = FindAttributes(node, result);

			foreach(var value in node.ChildNodes().OfType<EnumMemberDeclarationSyntax>())
			{
				string equals = value.EqualsValue?.ToString() ?? "";
				equals = Regex.Replace(equals, @" ?= ?", "=");
				result.Values[value.Identifier.ToString()] = equals;
			}

			return result;
		}
	}
}
