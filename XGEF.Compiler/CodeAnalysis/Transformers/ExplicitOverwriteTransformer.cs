﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;

namespace XGEF.Compiler
{
	public class ExplicitOverwriteTransformer : Transformer
	{
		private bool CheckForAttributes { get; set; }

		public override IEnumerable<ScriptFile> Transform(IEnumerable<ScriptFile> files)
		{
			foreach(ScriptFile file in files)
			{
				//First file can't have unmarked ancestors
				if (files.ToList().IndexOf(file) == 0)
					continue;

				//This checks that no member overwrites an earlier one without marking it [Overwrite]
				foreach (INode member in file.Members())
				{
					if (!CheckForAttributes && !member.HasAttributes)
					{
						//filter unsupported member types
						if (!GetSupportedMemberTypes().Contains(member.Type))
							continue;

						var (_, replacing) = FindMatchingAncestor(files, member);
						if (replacing != null)
							throw new UndeclaredOverwriteException($"{member.Type} {member.FullName} is hiding the same member from a lower priority file! If this is intended, it must be decorated with the [Overwrite] attribute.");
					}
				}
			}

			return files;
		}

		public ExplicitOverwriteTransformer() : this(false) { }
		public ExplicitOverwriteTransformer(bool attributeCheck) : base()
		{
			CheckForAttributes = attributeCheck;

			ValidOn = new List<AttributeTargets>
			{
				AttributeTargets.Class,
				AttributeTargets.Interface,
				AttributeTargets.Struct,
				AttributeTargets.Enum,
				AttributeTargets.Delegate,
				AttributeTargets.Method,
				AttributeTargets.Constructor,
				AttributeTargets.Property,

				AttributeTargets.Event,
				AttributeTargets.Field
			};
		}
	}
}
