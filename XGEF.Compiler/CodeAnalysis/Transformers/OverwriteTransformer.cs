﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;

namespace XGEF.Compiler
{
	public class OverwriteTransformer : Transformer<OverwriteAttribute>
	{
		public override IEnumerable<ScriptFile> Transform(IEnumerable<ScriptFile> files)
		{
			var ReplacementQueue = new List<KeyValuePair<INode, ScriptFile>>();
			foreach(ScriptFile file in files)
			{
				foreach (INode member in file.Members())
				{
					if(member.HasAttribute(AttributeName))
					{
						//Need to check that nothing marked [Overwrite] is missing the thing it's supposed to be overwriting.  
						Logger.Instance.Info($"Found {AttributeName} on {member.ToString()}.");
						ValidateAttributePlacement(member);

						var (oldFile, replacing) = FindMatchingAncestor(files, member);
						if (replacing == null)
							throw new InvalidAncestorException($"{member.Type} {member.FullName} has no valid ancestor to override!");

						ReplacementQueue.Add(new KeyValuePair<INode, ScriptFile>(replacing, oldFile));
						RemoveAttribute(member);
					}
				}
			}

			foreach(var pair in ReplacementQueue)
			{
				pair.Value.RemoveMember(pair.Key);
			}

			return files;
		}

		public OverwriteTransformer() : base()
		{
			ConflictingAttributes.Add("OverwriteAttribute");
			ConflictingAttributes.Add("ExtendAttribute");
		}
	}
}
