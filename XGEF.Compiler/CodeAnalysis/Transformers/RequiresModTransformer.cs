﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using XGEF.Logging;
using System.Text.RegularExpressions;

namespace XGEF.Compiler
{
	public class RequiresModTransformer : Transformer
	{

		protected override bool DoTransform(IEnumerable<ScriptFile> files, ScriptFile currentFile, INode member)
		{
			foreach (var attrib in member.GetAttributes(AttributeName).ToList())
			{
				if (attrib == null || attrib.Arguments.Count == 0 || attrib.Arguments.Count > 2)
					throw new InvalidAttributeException($"Attribute {AttributeName} on {member.FullName} in {member.Filename} has an invalid number of arguments!");

				string guid = attrib.Arguments[0].ToUpper().Replace("\"", "");
				bool exists = true;
				if (attrib.Arguments.Count == 2)
				{
					string arg = attrib.Arguments[1].Split(':')[1];
					exists = bool.Parse(arg);
				}

				if ((exists && !LoadedMods.Contains(guid)) || (!exists && LoadedMods.Contains(guid)))
				{
					if (exists)
						Logger.Instance.Warn($"Member {member.FullName} reports that it requires mod '{guid}', which was not loaded. This member is being unloaded.");
					else
						Logger.Instance.Warn($"Member {member.FullName} reports that it conflicts with mod '{guid}' which was loaded. This member is being unloaded.");
					currentFile.RemoveMember(member);
				}

				member.Attributes.Remove(attrib);

			}
			
			return true;
		}

		public RequiresModTransformer() : base()
		{
			Attribute = new RequiresModAttribute(Guid.Empty);
			ConflictingAttributes = new List<string>();
			Init();
		}
	}
}
