﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XGEF;
using XGEF.Compiler;
using XGEF.Stats;


namespace Engine.Stats
{
	#region something
	namespace Nested { }
	//1
	[Extend]
	//[Extend] //2
	public class StartingStats
	/* mod pre*/{
		public int NewTest { get; set; }

		[Extend]
		public enum Numbers { Three = 7, Five, Six = 7 }

		[RequiresMod("00000000-0000-0000-0000-000000000000", Exist:true)]
		[AppendFunction]
		static StartingStats()
		{
			SpeciesStats.Add(
				new StatInfo()
				{
					Name = "Gotcha Bitch",
					Abbreviation = "GB",
					Description = "",
					Notes = "A pain threshold that regenerates relatively quickly.  Injuries are more likely the less HP one has.",
					MinValue = 0,
					MaxValue = 1000,
					DataType = typeof(short),
					ModName = Constants.CoreModName
				});
		}

		public static void TestMethod(int a, int b)
		{

		}
	}/* mod post

	*/

#endregion

	//[type: Overwrite]
	//public class TestClass
	//{
	//	[method: Overwrite]
	//	public void TestMethod() { }

	//	[method: Overwrite]
	//	public TestClass() { }
	//}

	//[type: Overwrite]
	//public interface TestInterface { }

	//[type: Overwrite]
	//public struct TestStruct { }

	//[type: Overwrite]
	//public enum TestEnum { }

	//[type: Overwrite]
	//public delegate void TestDelegate();

}
