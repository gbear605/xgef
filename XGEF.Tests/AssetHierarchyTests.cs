﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Tests
{
	[TestClass()]
	public class AssetHierarchyTests
	{
		protected class TempDirectorySandbox : IDisposable
		{

			public TempDirectorySandbox()
			{
				MakeTempDirLayout();
			}

			#region IDisposable Support
			private bool disposedValue = false; // To detect redundant calls

			protected virtual void Dispose(bool disposing)
			{
				if (!disposedValue)
				{
					if (disposing)
					{
						ClearTempDirLayout();
					}

					// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
					// TODO: set large fields to null.

					disposedValue = true;
				}
			}

			// This code added to correctly implement the disposable pattern.
			public void Dispose()
			{
				// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
				Dispose(true);
				// TODO: uncomment the following line if the finalizer is overridden above.
				// GC.SuppressFinalize(this);
			}
			#endregion

			public string TempPath { get; protected set; }
			public string CorePath { get; protected set; }
			public string ModPath { get; protected set; }

			protected void MakeRandomTempDir()
			{
				if (TempPath != null)
					ClearTempDirLayout();

				TempPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

				while (Directory.Exists(TempPath))
				{
					TempPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
				}

				Directory.CreateDirectory(TempPath);
			}

			protected string MakeTempDirLayout()
			{
				MakeRandomTempDir();

				CorePath = Directory.CreateDirectory(Path.Combine(TempPath, "core_files")).FullName;
				ModPath = Directory.CreateDirectory(Path.Combine(TempPath, "mod_files")).FullName;

				for (int i = 1; i <= 5; i++)
				{
					string content = $"This is the content for file {i}";

					File.WriteAllText(Path.Combine(CorePath, $"game/files/file{i}.cs"), content);
					File.WriteAllText(Path.Combine(ModPath, $"game/files/file{i}.cs"), content);
				}

				return TempPath;
			}

			protected void ClearTempDirLayout()
			{
				Directory.Delete(TempPath, true);
				TempPath = null;
			}

		}

		//[TestMethod()]
		//public void AssetHierarchy_MultipleConflictingFilesAreReturnedInGetFiles()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		Assert.Fail();
		//	}
		//}

		//[TestMethod()]
		//public void AssetHierarchy_MultipleConflictingFilesReturnsOneFileInGetFile()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		Assert.Fail();
		//	}
		//}

		//[TestMethod()]
		//public void AssetHierarchy_TwoFilesOfDifferentPriorityAreSortedProperly()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		Assert.Fail();
		//	}
		//}

		//[TestMethod()]
		//public void AssetHierarchy_TwoFilesOfSamePriorityAreSortedProperly()
		//{
		//	using (var sandbox = new TempDirectorySandbox())
		//	{
		//		Assert.Fail();
		//	}
		//}
	}
}