﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF.Compiler;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace XGEF.Tests
{
	[TestClass]
	public class ScriptFileTests
	{
		[TestMethod]
		public void ScriptFile_ReconstructedCodeSameAsOriginal()
		{
			string original = File.ReadAllText("ScriptFile/TestScript.cs");
			original = ScriptFile.FilterText(original);
			
			ScriptFile result = ScriptFile.AnalyzeFile("test", original);
			string reconstructed = result.ReconstructCode();


			List<string> originalSplit = new List<string>(original.Split('\n'));
			List<string> reconstructSplit = new List<string>(reconstructed.Split('\n'));

			//we don't care about tab or whitespace quibbles.
			originalSplit = originalSplit.Select(x => Regex.Replace(x, @"\s+", "").ToString()).ToList();
			reconstructSplit = reconstructSplit.Select(x => Regex.Replace(x, @"\s+", "").ToString()).ToList();

			//Check that each line in each file contains an equivalant line in the other
			foreach (string line in originalSplit)
			{
				Assert.IsTrue(reconstructSplit.Contains(line), $"Found line in original that is not in reconstruction:\n{line}");
			}

			foreach (string line in reconstructSplit)
			{
				Assert.IsTrue(originalSplit.Contains(line), $"Found line in reconstruction that is not in original:\n{line}");
			}

			//Now check for uniqueness
			var reconstructCopy = reconstructSplit.ToList();
			foreach (string line in reconstructSplit)
			{
				Assert.IsTrue(reconstructCopy.Contains(line), $"Found unique line in original that is not in reconstruction:\n{line}");
				reconstructCopy.Remove(line);
			}

			var originalCopy = originalSplit.ToList();
			foreach (string line in originalSplit)
			{
				Assert.IsTrue(originalCopy.Contains(line), "Found unique line in reconstruction that is not in original:\n{line}");
				originalCopy.Remove(line);
			}

			Assert.AreEqual(reconstructCopy.Count, 0, 0, $"{reconstructCopy.Count} unaccounted for lines in the reconstruction.");
			Assert.AreEqual(originalCopy.Count, 0, 0, $"{originalCopy.Count} unaccounted for lines in the original.");
		}
	}
}
