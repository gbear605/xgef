﻿using System;

namespace Namespace
{
	public class Class<T> //comment
		where T : object
	{
		[Attribute]
		public Class() { }
		[Attribute("some value")]
		public Class(int i) { }
		[Attribute]
		public const int ConstantInt = 0;
		[Attribute]
		public int IntField;
		[Attribute]
		~Class() { }
		[Attribute]
		public void Method() { }
		[Attribute("some value")]
		public void Method2() => throw new NotImplementedException();
		[Attribute("some value")]
		public partial void PartialMethod(int i);
		[Attribute("some value")]
		public int Property { get; set; }
		[Attribute("some value")]
		public int Property2 => throw new NotImplementedException();
		[Attribute("some value")]
		public int this[int index]
		{
			get { return 0; }
			set { }
		}
		[Attribute("some value")]
		public static Class1 operator +(Class1 a, Class1 b)
		{
			return a;
		}
		public delegate void Delegate();
		[Attribute]
		public event Delegate Event;
		[Attribute]
		public class InnerClass { }
		[Attribute]
		public interface InnerInterface { }
		[Attribute]
		public struct InnerStruct { }
		[Attribute]
		public enum Enum { EnumVal1, EnumVal2 }
		[Attribute]
		public enum SetEnum { EnumVal1 = 1, EnumVal2 = 2 }

	}

	public delegate void Delegate();

	public interface Interface
	{
		int Function();
		int Property { get; }
	}
	public struct Struct
	{
		public const int ConstantInt = 0;
		public int IntField;
		public void Method() { }
		public int Property { get; set; }
		public int this[int index]
		{
			get { return 0; }
			set { }
		}
		public static Struct1 operator +(Struct1 a, Struct1 b)
		{
			return a;
		}
		public delegate void Delegate();
		public event Delegate Event;
		public class InnerClass { }
		public interface InnerInterface { }
		public struct InnerStruct { }
	}

	namespace NestedNamespace
	{
		public class Class1
		{
			public Class1() { }
			public const int ConstantInt = 0;
			public int IntField;
			~Class1() { }
			public void Method() { }
			public void Method2() => throw new NotImplementedException();
			public int Property { get; set; }
			public int this[int index]
			{
				get { return 0; }
				set { }
			}
			public static Class1 operator +(Class1 a, Class1 b)
			{
				return a;
			}
			public delegate void Delegate();
			public event Delegate Event;
			public class InnerClass { }
			public interface InnerInterface { }
			public struct InnerStruct { }

		}

		public delegate void Delegate();

		public interface Interface
		{
			int Function();
			int Property { get; }
			public event Delegate Event;
			public int this[int index] { get; set; }
		}
		public struct Struct
		{
			public const int ConstantInt = 0;
			public int IntField;
			public int PresetInt = 0;
			public void Method() { }
			public int Property { get; set; }
			public int this[int index]
			{
				get { return 0; }
				set { }
			}
			public static Struct1 operator +(Struct1 a, Struct1 b)
			{
				return a;
			}
			public delegate void Delegate();
			public event Delegate Event;
			public class InnerClass { }
			public interface InnerInterface { }
			public struct InnerStruct { }
			public enum SetEnum { EnumVal1 = 1, EnumVal2 = 2 }
		}
	}
	public enum SetEnum { EnumVal1 = 1, EnumVal2 = 2 }
}
