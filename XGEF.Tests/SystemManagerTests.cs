﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using Microsoft.VisualStudio.TestTools.UnitTesting;
using XGEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF.Tests
{
	[TestClass()]
	public class SystemManagerTests
	{
		[TestMethod()]
		public void SystemManager_RegisteringSystemSavesSystem()
		{
			TestSystem system = new TestSystem();
			SystemManager manager = new SystemManager();
			Assert.IsFalse(manager.Systems.ContainsKey(system.Name));
			manager.RegisterSystem(system);
			Assert.IsTrue(manager.Systems.ContainsKey(system.Name));
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void SystemManager_RegisteringSystemTwiceThrowsException()
		{
			TestSystem system = new TestSystem();
			SystemManager manager = new SystemManager();
			manager.RegisterSystem(system);
			manager.RegisterSystem(system);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SystemManager_RegisteringSystemAfterInitThrowsException()
		{
			TestSystem system = new TestSystem();
			SystemManager manager = new SystemManager();
			manager.Init();
			 manager.RegisterSystem(system);
		}

		[TestMethod()]
		public void SystemManager_InitializeFinalizes()
		{
			SystemManager manager = new SystemManager();
			Assert.IsFalse(manager.Initialized);
			manager.Init();
			Assert.IsTrue(manager.Initialized);
		}

		[TestMethod()]
		[ExpectedException(typeof(InvalidOperationException))]
		public void SystemManager_OnlyInitializesOnce()
		{
			SystemManager manager = new SystemManager();
			manager.Init();
			manager.Init();
		}

		[TestMethod()]
		public void SystemManager_InitializeCallsSystemFunctions()
		{
			TestSystem system = new TestSystem();

			bool preLoad = false;
			bool load = false;
			bool postLoad = false;

			system.RegisterCallback("PreLoad", () => { preLoad = true; });
			system.RegisterCallback("Load", () => { load = true; });
			system.RegisterCallback("PostLoad", () => { postLoad = true; });

			SystemManager manager = new SystemManager();
			manager.RegisterSystem(system);
			manager.Init();
			Assert.IsTrue(preLoad && load && postLoad);
		}
	}
}