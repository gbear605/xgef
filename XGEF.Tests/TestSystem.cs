﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF
{
	public class TestSystem : System
	{
		public Dictionary<string, Action> Callbacks { get; protected set; }

		public void RegisterCallback(string name, Action callback)
		{
			if(Callbacks.ContainsKey(name))
			{
				Callbacks[name] += callback;
			}
			else
			{
				Callbacks[name] = callback;
			}
		}

		public override void Init()
		{
			if (Callbacks.ContainsKey("Load"))
				Callbacks["Load"].Invoke();
		}

		public override void PostInit()
		{
			if (Callbacks.ContainsKey("PostLoad"))
				Callbacks["PostLoad"].Invoke();
		}

		public override void PostProcess()
		{
			if (Callbacks.ContainsKey("PostProcess"))
				Callbacks["PostProcess"].Invoke();
		}

		public override void PreInit()
		{
			if (Callbacks.ContainsKey("PreLoad"))
				Callbacks["PreLoad"].Invoke();
		}

		public override void PreProcess()
		{
			if (Callbacks.ContainsKey("PreProcess"))
				Callbacks["PreProcess"].Invoke();
		}

		public override void Process()
		{
			if (Callbacks.ContainsKey("Process"))
				Callbacks["Process"].Invoke();
		}

		public TestSystem()
		{
			Name = "test";
			Callbacks = new Dictionary<string, Action>();
		}
	}
}
