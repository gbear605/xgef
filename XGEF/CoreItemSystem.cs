﻿/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//////                                                                               ////
//////    Copyright 2017 Christian 'ketura' McCarty                                  ////
//////                                                                               ////
//////    Licensed under the Apache License, Version 2.0 (the "License");            ////
//////    you may not use this file except in compliance with the License.           ////
//////    You may obtain a copy of the License at                                    ////
//////                                                                               ////
//////                http://www.apache.org/licenses/LICENSE-2.0                     ////
//////                                                                               ////
//////    Unless required by applicable law or agreed to in writing, software        ////
//////    distributed under the License is distributed on an "AS IS" BASIS,          ////
//////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
//////    See the License for the specific language governing permissions and        ////
//////    limitations under the License.                                             ////
//////                                                                               ////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

//using System;

//namespace XGEF
//{
//	//This class is going to need to be chopped up into little bitty bits.	It is far too inflexible
//	// for what it will eventually need to be able to support.

//	public abstract class ModItemSystemBase : ModSystem<CoreItemSystem>
//	{
//		public abstract string TestMessage();
//	}

//	public class CoreItemSystem : CoreSystem<ModItemSystemBase>
//	{
//		public CoreItemSystem()
//		{
//			Name = "CoreItemSystem";
//			//this field should be superceded by the systems.cs file.
//			ModdedSystemPath = "engine/items/ModdedItemSystem.cs";
//			ModdedSystemName = "ModItemSystem";
//		}

//		public override void Init()
//		{
//			base.Init();
//			ModSystem.Init();
//		}

//		public void Message()
//		{
//			Console.WriteLine("Compiled message");
//		}


//		public override void Process()
//		{
//			//throw new NotImplementedException();
//		}
//	}

//	public interface IItem : ISystem
//	{
//		void Activate();
//		string Inspect();
//		string Description { get; set; }
//	}

//	//the original code, contained in /game/cs/items/Item.cs
//	public partial class Item : IItem
//	{
//		public Item()
//		{
//			//Init();
//		}

//		//[GeneratesEvent]
//		public void Activate()
//		{
//			//DoSomething();
//		}

//		public string Inspect()
//		{
//			return $"Name: {Description}";
//		}

//		public void Init()
//		{
//			throw new NotImplementedException();
//		}

//		public void Process()
//		{
//			throw new NotImplementedException();
//		}

//		public void PreInit()
//		{
//			throw new NotImplementedException();
//		}

//		public void PostInit()
//		{
//			throw new NotImplementedException();
//		}

//		public void PreProcess()
//		{
//			throw new NotImplementedException();
//		}

//		public void PostProcess()
//		{
//			throw new NotImplementedException();
//		}

//		public string Description { get; set; }

//		public string Name => throw new NotImplementedException();

//		public SystemManager Manager => throw new NotImplementedException();

	

//		public bool Initialized => throw new NotImplementedException();

//		SystemManager ISystem.Manager { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
//	}

//	//A mod that electronically signs all items so only the owner can use them
//	//Also makes it so if the battery drains to 0 the item explodes
//	//this is contained in /mods/AwesomeItemMod/game/cs/items/Item.cs
//	public partial class Item
//	{
//		//an added field
//		public int SignedOwner { get; protected set; }

//		//This and a handful of other events would naturally be available by default
//		//[AppendEvent(Creation)]
//		public void RegisterOwner()
//		{
//			//SignedOwner = this.Owner;
//		}

//		//[PrependEvent(Activate)]
//		public void BeforeItemUsed(EventArgs e)
//		{
//			//if (this.Owner != SignedOwner)
//				//e.Abort($"The item refuses to turn on.	Only {UnitName(SignedOwner)} can use it!");
//		}

//		//[AppendEvent(Activate)]
//		public void AfterItemUsed(EventArgs e)
//		{
//			//Some functions are defined in code and not in moddable scripts.	These three functions are thus not moddable (but might be prepend/appendable)
//			//if (ItemSystem.HasBattery(this) && ItemSystem.BatteryEmpty(this))
//			//{
//			//	ItemSystem.SelfDestruct(this);
//			//	e.Abort("The battery catastrophically failed!");
//			//}
//		}

//		//This was a mistake by the original devs to not have this generate an event, so we're overriding it and then appending our own message.
//		//[OverrideFunction]
//		//[GeneratesEvent]
//		public string Inspect2()
//		{
//			//string text = $"{Name}: {Description}";
//			//foreach (var caller in RegisteredEvents(Inspect))
//			//{
//			//	text += caller();
//			//}
//			return "";
//		}

//		//[AppendEvent(Inspect)]
//		public string AppendDescription()
//		{
//			return "This item appears to be electronically signed and locked.";
//		}
//	}
//}
