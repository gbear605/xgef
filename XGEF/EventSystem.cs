﻿///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
////                                                                               ////
////    Copyright 2017 Christian 'ketura' McCarty                                  ////
////                                                                               ////
////    Licensed under the Apache License, Version 2.0 (the "License");            ////
////    you may not use this file except in compliance with the License.           ////
////    You may obtain a copy of the License at                                    ////
////                                                                               ////
////                http://www.apache.org/licenses/LICENSE-2.0                     ////
////                                                                               ////
////    Unless required by applicable law or agreed to in writing, software        ////
////    distributed under the License is distributed on an "AS IS" BASIS,          ////
////    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   ////
////    See the License for the specific language governing permissions and        ////
////    limitations under the License.                                             ////
////                                                                               ////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XGEF
{
	

	public class EventSystem : System
	{
		public override void Init()
		{
			throw new NotImplementedException();
		}

		public override void Process()
		{
			throw new NotImplementedException();
		}

		public EventSystem(SystemManager manager)
		{
			Name = "EventSystem";
		}
	}

	public interface IEvent 
	{
		void Register(Delegate callback, Priority priority);
		void Unregister(Delegate callback, Priority priority);
		void UnRegisterAll(Delegate callback);
		void Invoke();
		void BeginInvoke();
	}

	public class Event<T> : IEvent
		where T : EventArgs //This might need to be replaced with a custom extension of EventArgs, to include
		// an abort
	{
		public Type ArgsType { get { return typeof(T); } }

		public Dictionary<Priority, HashSet<EventHandler<T>>> Callbacks { get; protected set; }

		public Event()
		{
			Callbacks = new Dictionary<Priority, HashSet<EventHandler<T>>>();

			foreach (var priority in new Priority().GetValues())
			{
				Callbacks[priority] = new HashSet<EventHandler<T>>();
			}
		}

		public bool HasPriority(Priority priority)
		{
			return Callbacks[priority].Count > 0;
		}

		public void Register(EventHandler<T> callback, Priority priority=Priority.Normal)
		{
			Callbacks[priority].Add(callback);
		}

		public void Register(Delegate callback, Priority priority = Priority.Normal)
		{
			if (callback is EventHandler<T>)
			{
				var handler = (EventHandler<T>)callback;
				Register(handler, priority);
			}
		}

		public void Unregister(EventHandler<T> callback, Priority priority = Priority.Normal)
		{
			Callbacks[priority].Remove(callback);
		}

		public void Unregister(Delegate callback, Priority priority = Priority.Normal)
		{
			if (callback is EventHandler<T>)
			{
				var handler = (EventHandler<T>)callback;
				Unregister(handler, priority);
			}
		}

		public void UnRegisterAll(EventHandler<T> callback)
		{
			foreach (var priority in new Priority().GetValues())
			{
				Unregister(callback, priority);
			}
		}

		public void UnRegisterAll(Delegate callback)
		{
			if (!(callback is EventHandler<T>))
				return;

			var handler = (EventHandler<T>)callback;
			foreach (var priority in new Priority().GetValues())
			{
				Unregister(handler, priority);
			}
		}

		public void Invoke()
		{
			//
		}

		public void BeginInvoke()
		{
			throw new NotImplementedException();
		}

		public static Event<T> operator +(Event<T> ev, EventHandler<T> callback)
		{
			ev.Register(callback);

			return ev;
		}
	}
}
