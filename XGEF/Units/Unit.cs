﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using XGEF.Stats;

namespace XGEF.Units
{
	[JsonObject]
	public class Unit
	{
		[JsonProperty]
		public Species BaseSpecies { get; }

		[JsonProperty]
		public StatContainer<LongStat> Stats { get; protected set; }
		public Unit(Species s)
		{
			BaseSpecies = s;

			Stats = new StatContainer<LongStat>();
			foreach(var stat in s.BaseStats.Values)
			{
				Stats[stat.Name] = new LongStat(stat.Name, (long)stat.Data);
			}
		}
	}
}
