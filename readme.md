# eXtensible Game Engine Framework (XGEF)

XGEF is a C# framework around which a game (or game engine) can be built.  The emphasis is on moddability and modularity, with built-in support for compiling \*.cs files into \*.dll's to be accessed by the game elsewhere.

If you have questions or would like to help contribute, please reach out to ketura#5732 on Discord.

## Getting Started

### Prerequisites

XGEF is built against .NET 4.6.2 and utilizes C# 7.1 features.  The solution is built in Visual Studio 2017 (although it will open in 2015), and is checked to work with Mono 4.6.2 or above.

### Installing

Simply download the repository and open the *.sln file in Visual Studio 2017.  In the event that nuget blows itself up, the included nuget packages and which projects they must be installed in are as follows:

```
Newtonsoft.Json (XGEF)
log4net (XGEF.Logging)
MSTest.TestAdapter (XGEF.Tests, XGEF.Tests.Stats)
MSTest.TestFramework (XGEF.Tests, XGEF.Tests.Stats)
Microsoft.Net.Compilers (XGEF, XGEF.Compiler)
Microsoft.CodeAnalysis.CSharp.Workspaces (XGEF.Compiler)
```

XGEF.TestApp includes a simple run-through of the major functions of XGEF's individual systems, including compiling.  If this program runs and produces a pair of 'Test System' *.dll's, then everything should be working properly.

### Reference Troubleshooting

In the event that DLL Hell raises its ugly head, the nuclear option may be required.  The steps for a more or less clean wipe of the solution are as follows:


- Uninstall all nuget packages
```
get-package | uninstall-package -removedependencies
```
- Delete the packages folder
- Perform a solution clean (Build menu -> Clean Solution)
- Go through all projects and delete all bin and obj folders
- Reinstall nuget packages as listed in the previous section to the latest stable build.  Do NOT install updates afterwards.
- Manually add references to the following libraries into XGEF.Compiler:
```
System.Collections.Immutable
System.Reflection
System.Runtime
System.Runtime.Extensions
System.Runtime.InteropServices
```
- If warning or errors indicating a mismatch of reference versions crop up, alter any needed <assemblyBinding> sections for these to the XGEF app.config.  The 'newVersion' attribute should be set to the primary required library version, which is usually the lower one in the case of mismatches.


## Contributing

Feel free to make pull requests.  Please note that new features are unlikely to be accepted if they do not fit within the current project plans.

## Authors

* **Christian 'ketura' McCarty** 


## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Special thanks to the denizens of the [/r/rational Discord server](https://discord.gg/sM99CF3) for their support and contributions.  In particular, a shoutout to Inferno Vulpix, Xavion, and Dwood.
